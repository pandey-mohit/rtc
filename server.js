require('dotenv').config()

const next = require('next')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const Chatkit = require('@pusher/chatkit-server')

const dev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

const app = next({ dev })
const handler = app.getRequestHandler()

const { INSTANCE_LOCATOR, SECRET_KEY } = process.env
const chatkit = new Chatkit.default({
  instanceLocator: INSTANCE_LOCATOR
, key: SECRET_KEY
})

app.prepare()
  .then(() => {
    const server = express()

    server.use(cors())
    server.use(bodyParser.json())
    server.use(bodyParser.urlencoded({ extended: true }))

    server.use('/service-worker.js', express.static(__dirname + '/service-worker.js'))

    server.get('*', (req, res) => {
      return handler(req, res)
    })

    server.post('/api/users', (req, res) => {
      const { user } = req.body

      chatkit.createUser({
        id: user
      , name: user
      })
      .then(() => res.sendStatus(201))
      .catch(error => {
        if (error.error === 'services/chatkit/user_already_exists') {
          res.sendStatus(200)
        } else {
          res.status(error.status).json(error)
        }
      })
    })
    
    server.post('/api/authenticate', (req, res) => {
      const authData = chatkit.authenticate({ userId: req.query.user_id })
      res.status(authData.status).send(authData.body)
    })

    server.listen(port, err => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })