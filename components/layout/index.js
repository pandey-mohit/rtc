import React, { Fragment } from 'react'
import Head from 'next/head'

const Layout = props => (
  <Fragment>
    <Head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <title>{ props.pageTitle || 'RTC Application' }</title>
    </Head>
    <div className="rtc">
      { props.children }
    </div>
    <style jsx global>
      {`
        body {
          margin: 0;
          padding: 0;
        }

        .rtc {
          padding: 20px;
          height: 100vh;
        }
      `}
    </style>
  </Fragment>
);

export default Layout;
