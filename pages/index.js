import React, { Component } from 'react'
import axios from 'axios'
import { Layout } from '../components'
import Chat from './chat'
import Login from './login'
// import '../service-worker'

class App extends Component {
  state = {
    user: ''
  , loader: true
  }

  componentDidMount() {
    const user = localStorage.getItem('user')
    if(user) {
      this.onSubmit(user)
    } else {
      this.setState({ loader: false })
    }
  }

  onSubmit = (user) => {
    axios.post('/api/users', { user })
    .then(response => {
      localStorage.setItem('user', user)
      this.setState({ user, loader: false })
    })
    .catch(error => console.error('error', error))
  }

  render() {
    const { user, loader } = this.state
   return (
     <Layout>
      { !loader && (!!user ? (
        <Chat user={user} />
      ) : (
        <Login onSubmit={this.onSubmit} />
      ))}
    </Layout>
   )
  }
}

export default () => (
  <App />
) 
