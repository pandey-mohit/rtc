import React, { Component, Fragment } from 'react'
import TextField from '@material-ui/core/TextField'
// import { makeStyles } from '@material-ui/core/styles'
// import Fab from '@material-ui/core/Fab'
// import ChevronRightIcon from '@material-ui/icons/ChevronRight'


class User extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    user: ''
  }

  onSubmit = (e)  => {
    e.preventDefault()
    this.props.onSubmit(this.state.user)
  }

  onChange = (e) => {
    this.setState({ user: e.target.value })
  }

  render() {
    return (
      <Fragment>
        <form onSubmit={this.onSubmit}>
          <TextField
            id="standard-textarea"
            label="Hello"
            placeholder="What is your name?"
            onChange={this.onChange}
          />
          {/* <Fab color="primary" aria-label="add">
            <ChevronRightIcon />
          </Fab> */}
          {/* <Icon className="fa-arrow-circle-right" color="primary"/>
          <ChevronRightOutlinedIcon /> */}
          <input type="submit" />
        </form>
      </Fragment>
    )
  }
}

export default User