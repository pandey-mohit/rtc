import React, { Component } from 'react'

const styles = {
  container: {
    padding: 20,
    borderTop: '1px #4C758F solid',
    marginBottom: 20,
  },
  form: {
    display: 'flex',
  },
  input: {
    color: 'inherit',
    background: 'none',
    outline: 'none',
    border: 'none',
    flex: 1,
    fontSize: 16,
  },
}

class SendMessage extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    text: ''
  }

  onSubmit =(e) => {
    e.preventDefault()
    this.props.onSubmit(this.state.text)
    this.setState({ text: '' })
  }

  onChange = (e) => {
    this.setState({ text: e.target.value })
    if (this.props.onChange) {
      this.props.onChange()
    }
  }

  render() {
    return (
      <div style={styles.container}>
        <div>
          <form onSubmit={this.onSubmit} style={styles.form}>
            <input
              type="text"
              placeholder="Type a message here then hit ENTER"
              onChange={this.onChange}
              value={this.state.text}
              style={styles.input}
            />
          </form>
        </div>
      </div>
    )
  }
}

export default SendMessage