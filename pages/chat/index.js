import React, { Component } from 'react'
import Chatkit from '@pusher/chatkit-client'
import MessagesList from './messagesList'
import SendMessage from './sendMessage'
import { OnlineUsers, TypingIndicator } from '../../components'

const styles = {
  container: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
  },
  chatContainer: {
    display: 'flex',
    flex: 1,
  },
  whosOnlineListContainer: {
    width: '300px',
    flex: 'none',
    padding: 20,
    backgroundColor: '#2c303b',
    color: 'white',
  },
  chatListContainer: {
    padding: 20,
    width: '85%',
    display: 'flex',
    flexDirection: 'column',
  },
}

class Chat extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    currentUser: {}
  , currentRoom: {}
  , messages: []
  , usersWhoAreTyping: []
  }

  componentDidMount() {
    const chatManager = new Chatkit.ChatManager({
      instanceLocator: 'v1:us1:f22f0849-b9b9-496b-8ba2-53e0cdb1d721'
    , userId: this.props.user
    , tokenProvider: new Chatkit.TokenProvider({
        url: '/api/authenticate'
      })
    })

    chatManager.connect()
    .then(currentUser => {
      this.setState({ currentUser })

      currentUser.enablePushNotifications({
        showNotificationsTabClosed: false
      , showNotificationsTabOpen: false
      })
      .then(() => {
        console.log('Push Notifications enabled')
      })
      .catch(error => {
        console.error("Push Notifications error:", error)
      })

      return currentUser.subscribeToRoom({
        roomId: '80fb1a1b-c449-4913-a5d2-21878e9181e6'
      , messageLimit: 100
      , hooks: {
          onMessage: message => {
            this.setState({
              messages: [...this.state.messages, message]
            })
          }
        , onUserStartedTyping: user => {
            this.setState({
              usersWhoAreTyping: [...this.state.usersWhoAreTyping, user.name]
           })
          }
        , onUserStoppedTyping: user => {
            this.setState({
              usersWhoAreTyping: this.state.usersWhoAreTyping.filter(
                username => username !== user.name
              )
            })
          }
        , onPresenceChange: () => this.forceUpdate()
        }
      })
    })
    .then(currentRoom => {
      this.setState({ currentRoom })
    })
    .catch(error => console.error('error', error))
  }

  sendTypingEvent = () => {
    this.state.currentUser
      .isTypingIn({ roomId: this.state.currentRoom.id })
      .catch(error => console.error('error', error))
  }

  sendMessage = (text) => {
    this.state.currentUser.sendMessage({
      text
    , roomId: this.state.currentRoom.id
    })
  }

  render() {
    const { messages } = this.state
    return (
      <div style={styles.container}>
        <div style={styles.chatContainer}>
          <aside style={styles.whosOnlineListContainer}>
            <OnlineUsers
              currentUser={this.state.currentUser}
              users={this.state.currentRoom.users}
            />
          </aside>
          <section style={styles.chatListContainer}>
          <MessagesList
            messages={messages}
            style={styles.chatList}
          />
          <TypingIndicator usersWhoAreTyping={this.state.usersWhoAreTyping} />
          <SendMessage
            onSubmit={this.sendMessage}
            onChange={this.sendTypingEvent}
          />
          </section>
        </div>
      </div>
    )
  }
}

export default Chat